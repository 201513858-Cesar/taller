const express = require("express");
const cors = require('cors')
const router = express();
const soap = require('soap');
const bodyParser = require('body-parser');
router.use(cors());

const url = 'http://www.dneonline.com/calculator.asmx?WSDL';
const clientPromise = new Promise((resolve, reject) => (
    soap.createClient(url, {}, (err, client) => err ? reject(err) : resolve(client))
));
const suma = ({ client, requests }) => Promise.all(requests.map(request => (
    new Promise((resolve, reject) => client.Add(request, (err, result) => (
        err ? reject(err) : resolve(result))
    ))
)));
const resta = ({ client, requests }) => Promise.all(requests.map(request => (
    new Promise((resolve, reject) => client.Subtract(request, (err, result) => (
        err ? reject(err) : resolve(result))
    ))
)));
const division = ({ client, requests }) => Promise.all(requests.map(request => (
    new Promise((resolve, reject) => client.Divide(request, (err, result) => (
        err ? reject(err) : resolve(result))
    ))
)));
const multiplicacion = ({ client, requests }) => Promise.all(requests.map(request => (
    new Promise((resolve, reject) => client.Multiply(request, (err, result) => (
        err ? reject(err) : resolve(result))
    ))
)));

router.use(bodyParser.json())
    .post('/suma', (req, res) => (clientPromise.then(client => ({ client, requests: req.body }))
        .then(suma)
        .then(results => {
            var resultado = results[0].AddResult;
            console.log("[SOAP]Add:: " + req.body[0].intA + " + " + req.body[0].intB + " = " + resultado);
            res.status(200).send({resultado})
        })
        .catch(({ message: error }) => res.status(500).send({ error }))
    ))
    .post('/resta', (req, res) => (clientPromise.then(client => ({ client, requests: req.body }))
        .then(resta)
        .then(results => {
            var resultado = results[0].SubtractResult;
            console.log("[SOAP]Subtract:: " + req.body[0].intA + " - " + req.body[0].intB + " = " + resultado);
            res.status(200).send({resultado})
        })
        .catch(({ message: error }) => res.status(500).send({ error }))
    ))
    .post('/division', (req, res) => (clientPromise.then(client => ({ client, requests: req.body }))
        .then(division)
        .then(results => {
            var resultado = results[0].DivideResult;
            console.log("[SOAP]Divide:: " + req.body[0].intA + " / " + req.body[0].intB + " = " + resultado);
            res.status(200).send({resultado})
        })
        .catch(({ message: error }) => res.status(500).send({ error }))
    ))
    .post('/multiplicacion', (req, res) => (clientPromise.then(client => ({ client, requests: req.body }))
        .then(multiplicacion)
        .then(results => {
            var resultado = results[0].MultiplyResult;
            console.log("[SOAP]Multiply:: " + req.body[0].intA + " * " + req.body[0].intB + " = " + resultado);
            res.status(200).send({resultado})
        })
        .catch(({ message: error }) => res.status(500).send({ error }))
    ))
    //.listen(3000, () => console.log('El servidor está inicializado en el puerto 3000'));

module.exports = router;